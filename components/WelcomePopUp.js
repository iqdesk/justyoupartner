import React, { useEffect } from 'react';
import { StyleSheet, View, Text, Dimensions, PermissionsAndroid } from 'react-native';
import axios from 'axios';
import auth from '@react-native-firebase/auth';
import { request, PERMISSIONS, requestNotifications } from 'react-native-permissions';

//Page 1 of the app, welcoming the user
const WelcomePopUpTrainer = ({ navigation }) => {
    useEffect(() => {
        requestAll()
    }, [])

    React.useEffect(() => {
        const unsubscribe = auth().onAuthStateChanged((user) => {
            global.covidAlert = true;
            if (user) {
                unsubscribe();
                navigation.navigate('TrainerContainer');
            } else {
                unsubscribe();
                navigation.navigate('GetStarted');
            }
        });
    }, []);

    const requestAll = async () => {
        if (Platform.OS === 'ios') {
            await request(PERMISSIONS.IOS.CAMERA);
            await request(PERMISSIONS.IOS.LOCATION_ALWAYS);
            await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
            await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
            await requestNotifications(['alert', 'sound']).then(({ status, settings }) => {
            });
        } else {
            await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS)

            await request(PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION);
            await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
            await request(PERMISSIONS.ANDROID.CALL_PHONE);
            await request(PERMISSIONS.ANDROID.CAMERA);
            await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
            await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
        }
        // const cameraStatus = await request(PERMISSIONS.IOS.CAMERA);
        // const contactsStatus = await request(PERMISSIONS.IOS.CONTACTS);
        // eturn {cameraStatus, contactsStatus};
    };



    return (
        <View style={styles.welcomePage1}>
            <View style={styles.textContainer}>
                <Text style={styles.justYoutitle}>Just You</Text>
                <Text style={styles.pertnerTitle}>Partner</Text>
            </View>
        </View>
    )
}



const styles = StyleSheet.create({
    welcomePage1: {
        backgroundColor: 'dodgerblue',
        height: Dimensions.get('window').height,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textContainer: {
        justifyContent: 'space-between',
        height: Dimensions.get('window').height * .15,
        alignItems: 'center'
    },
    justYoutitle: {
        fontSize: 90,
        color: 'white',
        fontWeight: 'bold'
    },
    pertnerTitle: {
        fontSize: 45,
        color: 'white',
        fontWeight: 'bold'
    }
});

export default WelcomePopUpTrainer;