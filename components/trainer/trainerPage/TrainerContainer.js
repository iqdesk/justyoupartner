import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, BackHandler } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import TrainerProfilePageStack from './trainerProfileTab/TrainerProfilePageStack';
import TrainerOrderPageStack from './trainerOrdersTab/TrainerOrderPageStack';
import TrainerStatsPageStack from './trainerStatsTab/TrainerStatsPageStack'
import TrainerCalendarPageStack from './trainerCalendarTab/TrainerCalendarPageStack'
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

const Tab = createBottomTabNavigator();

//The tab navigation container to handle the navigation in the trainer's area
const TrainerContainer = ({ navigation }) => {
    const [isFocused, setIsFocused] = useState(false)


    // useEffect(() => {
    //     const type = 'notification';
    //     PushNotificationIOS.addEventListener(type, onRemoteNotification);
    //     return () => {
    //         PushNotificationIOS.removeEventListener(type);
    //     };
    // });

    // const onRemoteNotification = (notification) => {

    //     console.log(notification);
    //     // if (isClicked) {
    //     //     // Navigate user to another screen
    //     // } else {
    //     //     // Do something else with push notification
    //     // }
    //     // Use the appropriate result based on what you needed to do for this notification
    //     const result = PushNotificationIOS.FetchResult.NoData;
    //     notification.finish(result);
    // };

    useEffect(() => {

        if (Platform.OS == 'android') {

        PushNotification.configure({
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: (token) => {
                console.log("push token -->", token);
                // let data = {
                //     PushToken: token.token,
                //     appSourceID: 1
                // };
                // dispatch(updatePushToken(data))
                //api.updatePushToken({Data: {PushToken: token.token}});
            },

            // (required) Called when a remote or local notification is opened or received
            onNotification: async (notification) => {
                console.log("notification ---->", notification);
                // if (notification.userInteraction || !notification.foreground) {
                //     if ((notification.data.msgType !== '3' && notification.data.msgType !== '10') || (notification.msgType !== '3' && notification.msgType !== '10')) {
                //         dispatch(getMessageTicket()).then(data => {


                //             let item = null;
                //             if (notification.data.msgType === '1' || notification.msgType === '1') {
                //                 let length1 = data.tblMessage1.length;
                //                 let length2 = data.tblMessage2.length;
                //                 let isFound = false;
                //                 for (let i = 0; i < length1; i++) {
                //                     if (
                //                         data.tblMessage1[i].id.toString() === notification.data.msgID || data.tblMessage1[i].id.toString() === notification.msgID
                //                     ) {
                //                         item = data.tblMessage1[i];
                //                         isFound = true;
                //                         break;
                //                     }
                //                 }
                //                 if (isFound) {
                //                     navigation.push('MessageDetails',
                //                         { stack: 'MyDrawer', back: 'Home', item },
                //                     );
                //                 } else {
                //                     let isFound2 = false;

                //                     for (let i = 0; i < length2; i++) {
                //                         if (
                //                             data.tblMessage2[i].id.toString() === notification.msgID
                //                         ) {
                //                             item = data.tblMessage2[i];
                //                             isFound2 = true;

                //                             break;
                //                         }
                //                     }
                //                     if (isFound2) {
                //                         navigation.push('MessageDetails',
                //                             { stack: 'MyDrawer', back: 'Home', item },
                //                         );
                //                     }
                //                 }
                //             } else if (notification.msgType === '2' || notification.data.msgType === '2') {
                //                 let length = data.tblTicket.length;
                //                 for (let i = 0; i < length; i++) {
                //                     if (data.tblTicket[i].Id.toString() === notification.msgID || data.tblTicket[i].Id.toString() === notification.data.msgID) {
                //                         item = data.tblTicket[i];
                //                         break;
                //                     }
                //                 }
                //                 if (item.Ticket_Status === 3) {
                //                     navigation.navigate('HomeStack', {
                //                         screen: 'TicketInfoClose',
                //                         params: { stack: 'HomeStack', ticketId: item.Id, back: 'TicketStatus' },
                //                     });
                //                 } else {
                //                     navigation.navigate('HomeStack', {
                //                         screen: 'TicketInfoOpen',
                //                         params: { stack: 'HomeStack', ticketId: item.Id, back: 'TicketStatus' },
                //                     });
                //                 }
                //             }
                //         });
                //     }

                // } else {
                //     await dispatch(getMessageTicket());
                //     // this.setState({ isShowAlert: true });
                // }
                // process the notification

                // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/react-native-push-notification-ios)
                // notification.finish(PushNotificationIOS.FetchResult.NoData);
            },

            // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
            senderID: '875902311914',

            // IOS ONLY (optional): default: all - Permissions to register.
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },

            // Should the initial notification be popped automatically
            // default: true
            popInitialNotification: true,

            /**
             * (optional) default: true
             * - Specified if permissions (ios) and token (android and ios) will requested or not,
             * - if not, you must call PushNotificationsHandler.requestPermissions() later
             */
            requestPermissions: true,
            largeIcon: 'ic_launcher',
            smallIcon: 'ic_notification',
        });
        }

        if (Platform.OS == 'ios') {
            PushNotificationIOS.requestPermissions();
            PushNotificationIOS.addEventListener('register', _onRegistered);
            PushNotificationIOS.addEventListener('registrationError', _onRegistrationError);
            PushNotificationIOS.addEventListener('notification', _onRemoteNotification);
            PushNotificationIOS.addEventListener('localNotification', _onLocalNotification);
            return () => {
                PushNotificationIOS.removeEventListener('register');
                PushNotificationIOS.removeEventListener('registrationError');
                PushNotificationIOS.removeEventListener('notification');
                PushNotificationIOS.removeEventListener('localNotification');

            };
        }
        navigation.addListener('focus', () => {
            setIsFocused(true);
        });

        navigation.addListener('blur', () => {
            setIsFocused(false);
        });


    }, [isFocused])


    const _onRegistered = async (token) => {
        messaging()
            .getToken()
            .then((tk) => {
                console.log("token ----->", tk);
                // dispatch(updatePushToken({
                //     PushToken: tk,
                //     appSourceID: 1
                // }));
            });

        messaging().onMessage(async (remoteMessage) => {
            console.log(remoteMessage);

        });

        messaging().onNotificationOpenedApp(async (remoteMessage) => {
            console.log(remoteMessage);
            if (Platform.OS === 'ios') {
                const notification = remoteMessage.data;
                console.log("notification -->", notification);
                // if (notification.msgType !== '3' && notification.msgType !== '10') {
                //     await dispatch(getMessageTicket());
                //     let item = null;
                //     if (notification.msgType === '1') {
                //         let length1 = messageTicket.tblMessage1.length;
                //         let length2 = messageTicket.tblMessage2.length;
                //         let isFound = false;
                //         for (let i = 0; i < length1; i++) {
                //             if (messageTicket.tblMessage1[i].id.toString() === notification.msgID) {
                //                 item = messageTicket.tblMessage1[i];
                //                 isFound = true;
                //                 break;
                //             }
                //         }
                //         if (isFound) {
                //             navigation.navigate('HomeStack', {
                //                 screen: 'MessageDetails',
                //                 params: { stack: 'MyDrawer', back: 'Home', item },
                //             });
                //         } else {
                //             for (let i = 0; i < length2; i++) {
                //                 if (messageTicket.tblMessage2[i].id.toString() === notification.msgID) {
                //                     item = messageTicket.tblMessage2[i];
                //                     break;
                //                 }
                //             }
                //             navigation.navigate('HomeStack', {
                //                 screen: 'MessageDetails',
                //                 params: { stack: 'MyDrawer', back: 'Home', item },
                //             });
                //         }
                //     } else if (notification.msgType === '2' || notification.data.msgType === '2') {
                //         let length = messageTicket.tblTicket.length;
                //         for (let i = 0; i < length; i++) {
                //             if (messageTicket.tblTicket[i].Id.toString() === notification.msgID || messageTicket.tblTicket[i].Id.toString() === notification.data.msgID) {
                //                 item = messageTicket.tblTicket[i];
                //                 break;
                //             }
                //         }
                //         console.log("item found --->", item);
                //         if (item.Ticket_Status === 3) {
                //             navigation.navigate('HomeStack', {
                //                 screen: 'TicketInfoClose',
                //                 params: { stack: 'MyDrawer', ticketId: item.Id },
                //             });
                //         } else {
                //             navigation.navigate('HomeStack', {
                //                 screen: 'TicketInfoOpen',
                //                 params: { stack: 'MyDrawer', ticketId: item.Id },
                //             });
                //         }
                //     }
                // }
            }
        });
    };

    const _onRegistrationError = (error) => { };

    const _onLocalNotification = (notification) => { };

    const _onRemoteNotification = (notification) => {
        console.log("notification", notification);

    };


    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', () => true)
        return () =>
            BackHandler.removeEventListener('hardwareBackPress', () => true)
    }, [])

    return (
        <Tab.Navigator
            tabBarOptions={{
                style: {
                    height: 60,
                    borderTopWidth: 1,
                    paddingTop: 10
                },
                showLabel: false,
            }}
        >
            <Tab.Screen
                name="TrainerProfilePageStack"
                component={TrainerProfilePageStack}
                options={{
                    tabBarIcon: ({ focused, tintColor }) => (
                        !focused ? <Image
                            source={require('../../../images/profilePageIcon.png')}
                            style={[styles.profileIcon, { tintColor: tintColor }]}
                        />
                            :
                            <Image
                                source={require('../../../images/profilePageIconFocused.png')}
                                style={[styles.profileFocusedIcon, { tintColor: tintColor }]}
                            />
                    )
                }}
            />
            <Tab.Screen
                name="TrainerOrderPageStack"
                component={TrainerOrderPageStack}
                options={{
                    tabBarIcon: ({ focused, tintColor }) => (
                        !focused ? <Image
                            source={require('../../../images/ordersIcon.png')}
                            style={[styles.ordersIcon, { tintColor: tintColor }]}
                        />
                            :
                            <Image
                                source={require('../../../images/ordersIconFocused.png')}
                                style={[styles.ordersFocusedIcon, { tintColor: tintColor }]}
                            />
                    ),
                }}
            />
            <Tab.Screen
                name="TrainerStatsPageStack"
                component={TrainerStatsPageStack}
                options={{
                    tabBarIcon: ({ focused, tintColor }) => (
                        !focused ? <Image
                            source={require('../../../images/statsIcon.png')}
                            style={[styles.statsIcon, { tintColor: tintColor }]}
                        />
                            :
                            <Image
                                source={require('../../../images/statsFocusedIcon.png')}
                                style={[styles.statsFocusedIcon, { tintColor: tintColor }]}
                            />
                    ),
                }}
            />
            <Tab.Screen
                name="TrainerCalendarPageStack"
                component={TrainerCalendarPageStack}
                options={{
                    tabBarIcon: ({ focused, tintColor }) => (
                        !focused ? <Image
                            source={require('../../../images/calendarIconGray.jpg')}
                            style={[styles.statsIcon, { tintColor: tintColor }]}
                        />
                            :
                            <Image
                                source={require('../../../images/calendarFocusedIcon.jpeg')}
                                style={[styles.statsFocusedIcon, { tintColor: tintColor }]}
                            />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

const styles = StyleSheet.create({
    profileIcon: {
        width: 25,
        height: 25
    },
    profileFocusedIcon: {
        width: 30,
        height: 30,
    },
    ordersIcon: {
        width: 25,
        height: 25,
    },
    ordersFocusedIcon: {
        width: 30,
        height: 30,
    },
    statsIcon: {
        width: 25,
        height: 25,
    },
    statsFocusedIcon: {
        width: 30,
        height: 30,
    }
});

export default TrainerContainer;