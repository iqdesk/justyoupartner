# Diff Details

Date : 2023-07-13 11:05:12

Directory /Users/assafscialom/Documents/justYouPartner.nosync/justyoupartner

Total : 7 files,  23 codes, 0 comments, 0 blanks, all 23 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | 2 | 0 | 0 | 2 |
| [android/app/src/main/res/values/colors.xml](/android/app/src/main/res/values/colors.xml) | XML | 3 | 0 | 0 | 3 |
| [android/build.gradle](/android/build.gradle) | Groovy | 0 | 0 | 1 | 1 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | 2 | 0 | -1 | 1 |
| [components/trainer/trainerRegister/LogInTrainer.js](/components/trainer/trainerRegister/LogInTrainer.js) | JavaScript | 1 | 0 | 0 | 1 |
| [package-lock.json](/package-lock.json) | JSON | 14 | 0 | 0 | 14 |
| [package.json](/package.json) | JSON | 1 | 0 | 0 | 1 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details