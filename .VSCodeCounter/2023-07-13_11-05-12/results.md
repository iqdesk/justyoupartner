# Summary

Date : 2023-07-13 11:05:12

Directory /Users/assafscialom/Documents/justYouPartner.nosync/justyoupartner

Total : 206 files,  84151 codes, 1463 comments, 2631 blanks, all 88245 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 35 | 63,322 | 0 | 17 | 63,339 |
| JavaScript | 151 | 20,191 | 1,297 | 2,475 | 23,963 |
| XML | 6 | 152 | 4 | 14 | 170 |
| Groovy | 3 | 142 | 116 | 30 | 288 |
| Java | 3 | 129 | 25 | 21 | 175 |
| Objective-C | 3 | 112 | 2 | 34 | 148 |
| Batch | 1 | 76 | 0 | 25 | 101 |
| Diff | 1 | 13 | 0 | 1 | 14 |
| Java Properties | 2 | 9 | 19 | 10 | 38 |
| C++ | 1 | 5 | 0 | 4 | 9 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 206 | 84,151 | 1,463 | 2,631 | 88,245 |
| . (Files) | 11 | 35,503 | 14 | 25 | 35,542 |
| __tests__ | 1 | 7 | 4 | 4 | 15 |
| android | 43 | 28,367 | 163 | 112 | 28,642 |
| android (Files) | 4 | 117 | 24 | 39 | 180 |
| android/app | 38 | 28,245 | 139 | 72 | 28,456 |
| android/app (Files) | 2 | 187 | 111 | 25 | 323 |
| android/app/src | 36 | 28,058 | 28 | 47 | 28,133 |
| android/app/src/debug | 2 | 65 | 8 | 9 | 82 |
| android/app/src/debug (Files) | 1 | 6 | 0 | 3 | 9 |
| android/app/src/debug/java | 1 | 59 | 8 | 6 | 73 |
| android/app/src/debug/java/com | 1 | 59 | 8 | 6 | 73 |
| android/app/src/debug/java/com/justyoutrainer | 1 | 59 | 8 | 6 | 73 |
| android/app/src/main | 34 | 27,993 | 20 | 38 | 28,051 |
| android/app/src/main (Files) | 1 | 78 | 1 | 6 | 85 |
| android/app/src/main/java | 2 | 70 | 17 | 15 | 102 |
| android/app/src/main/java/com | 2 | 70 | 17 | 15 | 102 |
| android/app/src/main/java/com/justyoutrainer | 2 | 70 | 17 | 15 | 102 |
| android/app/src/main/res | 31 | 27,845 | 2 | 17 | 27,864 |
| android/app/src/main/res/raw | 28 | 27,834 | 0 | 13 | 27,847 |
| android/app/src/main/res/values | 3 | 11 | 2 | 4 | 17 |
| android/gradle | 1 | 5 | 0 | 1 | 6 |
| android/gradle/wrapper | 1 | 5 | 0 | 1 | 6 |
| components | 78 | 19,187 | 1,279 | 2,165 | 22,631 |
| components (Files) | 3 | 233 | 8 | 17 | 258 |
| components/globalComponents | 11 | 956 | 17 | 100 | 1,073 |
| components/globalComponents (Files) | 7 | 366 | 6 | 39 | 411 |
| components/globalComponents/calendar | 4 | 590 | 11 | 61 | 662 |
| components/place | 16 | 2,780 | 233 | 184 | 3,197 |
| components/place/placeRegister | 16 | 2,780 | 233 | 184 | 3,197 |
| components/trainer | 48 | 15,218 | 1,021 | 1,864 | 18,103 |
| components/trainer/trainerPage | 29 | 9,726 | 696 | 1,344 | 11,766 |
| components/trainer/trainerPage (Files) | 1 | 123 | 1 | 6 | 130 |
| components/trainer/trainerPage/trainerCalendarTab | 3 | 811 | 127 | 188 | 1,126 |
| components/trainer/trainerPage/trainerOrdersTab | 5 | 2,041 | 129 | 306 | 2,476 |
| components/trainer/trainerPage/trainerProfileTab | 18 | 6,031 | 336 | 749 | 7,116 |
| components/trainer/trainerPage/trainerStatsTab | 2 | 720 | 103 | 95 | 918 |
| components/trainer/trainerRegister | 19 | 5,492 | 325 | 520 | 6,337 |
| context | 34 | 520 | 0 | 169 | 689 |
| context (Files) | 2 | 31 | 0 | 8 | 39 |
| context/orderContexts | 1 | 18 | 0 | 5 | 23 |
| context/placeContextes | 13 | 194 | 0 | 64 | 258 |
| context/trainerContextes | 18 | 277 | 0 | 92 | 369 |
| ios | 7 | 242 | 3 | 41 | 286 |
| ios/justyoutrainer | 6 | 191 | 3 | 26 | 220 |
| ios/justyoutrainer (Files) | 4 | 123 | 3 | 24 | 150 |
| ios/justyoutrainer/Images.xcassets | 2 | 68 | 0 | 2 | 70 |
| ios/justyoutrainer/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/justyoutrainer/Images.xcassets/AppIcon.appiconset | 1 | 62 | 0 | 1 | 63 |
| ios/justyoutrainerTests | 1 | 51 | 0 | 15 | 66 |
| patches | 1 | 13 | 0 | 1 | 14 |
| reducers | 31 | 312 | 0 | 114 | 426 |
| reducers (Files) | 1 | 9 | 0 | 3 | 12 |
| reducers/orderReducers | 1 | 13 | 0 | 9 | 22 |
| reducers/placeReducers | 12 | 117 | 0 | 39 | 156 |
| reducers/trainerReducers | 17 | 173 | 0 | 63 | 236 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)