# Diff Summary

Date : 2023-07-13 11:05:12

Directory /Users/assafscialom/Documents/justYouPartner.nosync/justyoupartner

Total : 7 files,  23 codes, 0 comments, 0 blanks, all 23 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 2 | 15 | 0 | 0 | 15 |
| Groovy | 3 | 4 | 0 | 0 | 4 |
| XML | 1 | 3 | 0 | 0 | 3 |
| JavaScript | 1 | 1 | 0 | 0 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 7 | 23 | 0 | 0 | 23 |
| . (Files) | 2 | 15 | 0 | 0 | 15 |
| android | 4 | 7 | 0 | 0 | 7 |
| android (Files) | 2 | 2 | 0 | 0 | 2 |
| android/app | 2 | 5 | 0 | 0 | 5 |
| android/app (Files) | 1 | 2 | 0 | 0 | 2 |
| android/app/src | 1 | 3 | 0 | 0 | 3 |
| android/app/src/main | 1 | 3 | 0 | 0 | 3 |
| android/app/src/main/res | 1 | 3 | 0 | 0 | 3 |
| android/app/src/main/res/values | 1 | 3 | 0 | 0 | 3 |
| components | 1 | 1 | 0 | 0 | 1 |
| components/trainer | 1 | 1 | 0 | 0 | 1 |
| components/trainer/trainerRegister | 1 | 1 | 0 | 0 | 1 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)