# Summary

Date : 2023-07-13 10:58:44

Directory /Users/assafscialom/Documents/justYouPartner.nosync/justyoupartner

Total : 205 files,  84128 codes, 1463 comments, 2631 blanks, all 88222 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 35 | 63,307 | 0 | 17 | 63,324 |
| JavaScript | 151 | 20,190 | 1,297 | 2,475 | 23,962 |
| XML | 5 | 149 | 4 | 14 | 167 |
| Groovy | 3 | 138 | 116 | 30 | 284 |
| Java | 3 | 129 | 25 | 21 | 175 |
| Objective-C | 3 | 112 | 2 | 34 | 148 |
| Batch | 1 | 76 | 0 | 25 | 101 |
| Diff | 1 | 13 | 0 | 1 | 14 |
| Java Properties | 2 | 9 | 19 | 10 | 38 |
| C++ | 1 | 5 | 0 | 4 | 9 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 205 | 84,128 | 1,463 | 2,631 | 88,222 |
| . (Files) | 11 | 35,488 | 14 | 25 | 35,527 |
| __tests__ | 1 | 7 | 4 | 4 | 15 |
| android | 42 | 28,360 | 163 | 112 | 28,635 |
| android (Files) | 4 | 115 | 24 | 39 | 178 |
| android/app | 37 | 28,240 | 139 | 72 | 28,451 |
| android/app (Files) | 2 | 185 | 111 | 25 | 321 |
| android/app/src | 35 | 28,055 | 28 | 47 | 28,130 |
| android/app/src/debug | 2 | 65 | 8 | 9 | 82 |
| android/app/src/debug (Files) | 1 | 6 | 0 | 3 | 9 |
| android/app/src/debug/java | 1 | 59 | 8 | 6 | 73 |
| android/app/src/debug/java/com | 1 | 59 | 8 | 6 | 73 |
| android/app/src/debug/java/com/justyoutrainer | 1 | 59 | 8 | 6 | 73 |
| android/app/src/main | 33 | 27,990 | 20 | 38 | 28,048 |
| android/app/src/main (Files) | 1 | 78 | 1 | 6 | 85 |
| android/app/src/main/java | 2 | 70 | 17 | 15 | 102 |
| android/app/src/main/java/com | 2 | 70 | 17 | 15 | 102 |
| android/app/src/main/java/com/justyoutrainer | 2 | 70 | 17 | 15 | 102 |
| android/app/src/main/res | 30 | 27,842 | 2 | 17 | 27,861 |
| android/app/src/main/res/raw | 28 | 27,834 | 0 | 13 | 27,847 |
| android/app/src/main/res/values | 2 | 8 | 2 | 4 | 14 |
| android/gradle | 1 | 5 | 0 | 1 | 6 |
| android/gradle/wrapper | 1 | 5 | 0 | 1 | 6 |
| components | 78 | 19,186 | 1,279 | 2,165 | 22,630 |
| components (Files) | 3 | 233 | 8 | 17 | 258 |
| components/globalComponents | 11 | 956 | 17 | 100 | 1,073 |
| components/globalComponents (Files) | 7 | 366 | 6 | 39 | 411 |
| components/globalComponents/calendar | 4 | 590 | 11 | 61 | 662 |
| components/place | 16 | 2,780 | 233 | 184 | 3,197 |
| components/place/placeRegister | 16 | 2,780 | 233 | 184 | 3,197 |
| components/trainer | 48 | 15,217 | 1,021 | 1,864 | 18,102 |
| components/trainer/trainerPage | 29 | 9,726 | 696 | 1,344 | 11,766 |
| components/trainer/trainerPage (Files) | 1 | 123 | 1 | 6 | 130 |
| components/trainer/trainerPage/trainerCalendarTab | 3 | 811 | 127 | 188 | 1,126 |
| components/trainer/trainerPage/trainerOrdersTab | 5 | 2,041 | 129 | 306 | 2,476 |
| components/trainer/trainerPage/trainerProfileTab | 18 | 6,031 | 336 | 749 | 7,116 |
| components/trainer/trainerPage/trainerStatsTab | 2 | 720 | 103 | 95 | 918 |
| components/trainer/trainerRegister | 19 | 5,491 | 325 | 520 | 6,336 |
| context | 34 | 520 | 0 | 169 | 689 |
| context (Files) | 2 | 31 | 0 | 8 | 39 |
| context/orderContexts | 1 | 18 | 0 | 5 | 23 |
| context/placeContextes | 13 | 194 | 0 | 64 | 258 |
| context/trainerContextes | 18 | 277 | 0 | 92 | 369 |
| ios | 7 | 242 | 3 | 41 | 286 |
| ios/justyoutrainer | 6 | 191 | 3 | 26 | 220 |
| ios/justyoutrainer (Files) | 4 | 123 | 3 | 24 | 150 |
| ios/justyoutrainer/Images.xcassets | 2 | 68 | 0 | 2 | 70 |
| ios/justyoutrainer/Images.xcassets (Files) | 1 | 6 | 0 | 1 | 7 |
| ios/justyoutrainer/Images.xcassets/AppIcon.appiconset | 1 | 62 | 0 | 1 | 63 |
| ios/justyoutrainerTests | 1 | 51 | 0 | 15 | 66 |
| patches | 1 | 13 | 0 | 1 | 14 |
| reducers | 31 | 312 | 0 | 114 | 426 |
| reducers (Files) | 1 | 9 | 0 | 3 | 12 |
| reducers/orderReducers | 1 | 13 | 0 | 9 | 22 |
| reducers/placeReducers | 12 | 117 | 0 | 39 | 156 |
| reducers/trainerReducers | 17 | 173 | 0 | 63 | 236 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)